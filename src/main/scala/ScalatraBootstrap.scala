import com.googlecode.flyway.core.Flyway
import com.mchange.v2.c3p0.ComboPooledDataSource
import michalz.messagewall._
import org.scalatra._
import javax.servlet.ServletContext

class ScalatraBootstrap extends LifeCycle {

  val dataSource = new ComboPooledDataSource

  override def init(context: ServletContext) {

    val flyway: Flyway = new Flyway
    flyway.setDataSource(dataSource)
    flyway.migrate

    context.mount(new MessageWallServlet, "/*")
  }

  override def destroy(context: ServletContext) {
    super.destroy(context)
    dataSource.close()
  }
}
