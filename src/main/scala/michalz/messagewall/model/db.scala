package michalz.messagewall.model.db;

import scala.slick.driver.PostgresDriver.simple.Table;

object User extends Table[(Long, String, String, Option[String])]("USERS") {
  def id = column[Long]("USER_ID", O.PrimaryKey)
  def userName = column[String]("USER_NAME")
  def passwordHash = column[String]("PASSWORD_HASH")
  def email = column[Option[String]]("EMAIL")
  def * = id ~ userName ~ passwordHash ~ email
}

