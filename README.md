# Message Wall Application #

## Build & Run ##

```sh
$ cd Message_Wall_Application
$ ./sbt
> container:start
> browse
```

If `browse` doesn't launch your browser, manually open [http://localhost:8080/](http://localhost:8080/) in your browser.
