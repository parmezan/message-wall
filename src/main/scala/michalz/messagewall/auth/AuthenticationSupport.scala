package michalz.messagewall.auth

import org.scalatra.auth.{ScentryConfig, ScentrySupport}
import michalz.messagewall.model.User
import org.scalatra.auth.strategy.BasicAuthSupport
import org.scalatra.ScalatraBase

/**
 * @author Michał T. Zając <parmezan@gmail.com>
 * @since 2013.10.26
 */
trait AuthenticationSupport extends ScentrySupport[User] with BasicAuthSupport[User] {
  self: ScalatraBase =>
  def realm: String = "Basic Scalatra Realm"

  protected def fromSession: PartialFunction[String, User] = { case id: String => User.getUser(id).get }
  protected def toSession: PartialFunction[User, String] = { case user: User => user.userName }

  protected val scentryConfig = (new ScentryConfig {}).asInstanceOf[ScentryConfiguration]

  override protected def configureScentry = {
    scentry.unauthenticated {
      scentry.strategies("Basic").unauthenticated()
    }
  }

  override protected def registerAuthStrategies = {
    scentry.register("Basic", app => new PasswordBasedAuthStrategy(app, realm))
  }
}
