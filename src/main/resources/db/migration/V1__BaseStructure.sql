
create table users (
  user_id bigserial primary key,
  user_name varchar(255) not null unique,
  password_hash varchar(255) not null,
  email varchar(1024)
);

create table posts (
  post_id bigserial primary key,
  reference_id bigint,
  title varchar(1024) not null,
  posted timestamp not null default now(),
  author_id bigint,
  content text,
  constraint reference_id_fk foreign key (reference_id) references posts(post_id),
  constraint author_id_fk foreign key (author_id) references users(user_id)
);

create index posts_title_key on posts USING BTREE (title);