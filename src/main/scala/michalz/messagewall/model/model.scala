package michalz.messagewall.model

import java.security.MessageDigest

object User {
  lazy val md = MessageDigest.getInstance("SHA-256")

  def makePasswordHash(userName: String, password: String): String = {
    val passwordWithSalt: String = String.format("%s$(%s)$", password, userName)
    md.digest(passwordWithSalt.getBytes).map("%02x".format(_)).mkString
  }

  def getUser(userName: String) = {
    if(userName == "admin") Some(User("admin", Some("admin@gdzies.com"), makePasswordHash("admin", "sernik")))
    else None
  }
}

case class User(userName: String, email: Option[String], passwordHash: String) {

  def getPasswordHash(password: String): String = {
    User.makePasswordHash(userName, password)
  }

  def checkPassword(password: String) = passwordHash == getPasswordHash(password)

  def changePassword(newPassword: String): User = User(this.userName, this.email, getPasswordHash(newPassword))
}