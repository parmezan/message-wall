package michalz.messagewall

import org.scalatra._
import scalate.ScalateSupport
import michalz.messagewall.model.User

class MessageWallServlet extends MessageWallApplicationStack {

  get("/") {
    <html>
      <body>
        <h1>Hello, world!</h1>
        Say <a href="hello-scalate">hello to Scalate</a>.
      </body>
    </html>
  }

  get("/protected") {
    basicAuth
    <html>
       <body>
         <h1>Protected resource</h1>
         <p>You are authenticated</p>
       </body>
    </html>
  }
  
}
