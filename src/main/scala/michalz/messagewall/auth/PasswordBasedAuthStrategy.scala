package michalz.messagewall.auth

import org.scalatra.auth.strategy.BasicAuthStrategy
import michalz.messagewall.model.User
import org.scalatra.ScalatraBase
import javax.servlet.http.{HttpServletResponse, HttpServletRequest}
import org.slf4j.{Logger, LoggerFactory}

/**
 * @author Michał T. Zając <parmezan@gmail.com>
 * @since 2013.10.26
 */
class PasswordBasedAuthStrategy(protected override val app: ScalatraBase, realm: String)
  extends BasicAuthStrategy[User](app, realm) {

  private val logger: Logger = LoggerFactory.getLogger(classOf[PasswordBasedAuthStrategy])

  protected def getUserId(user: User)(implicit request: HttpServletRequest, response: HttpServletResponse): String = user.userName

  protected def validate(userName: String, password: String)(implicit request: HttpServletRequest, response: HttpServletResponse): Option[User] = {
    logger.info("Authentication is called user: %s, password: %s".format(userName, password))
    val user: Option[User] = User.getUser(userName)
    user.filter(_.checkPassword(password))
  }
}
